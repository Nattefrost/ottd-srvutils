#!/bin/bash

# shellcheck source=/dev/null
source "${HOME}/ottd-srvutils/config.sh"

colorize

no_server_running || exit 1

if [ -z "${1}" ]
then
  echo "${cyan}Info::${nc}Launching OpenTTD with default config file in GNU screen session 'ttd'"
  screen -L -Logfile "${server_logfile}" -dmS ttd "${HOME}/openttd/openttd" -D
else
  echo "${cyan}Info::${nc}Launching OpenTTD with ${1} config file in GNU screen session 'ttd'"
  echo "${magenta}Debug::${nc}CLI 'screen -L -Logfile ${server_logfile} -dmS ttd /home/${USER}/openttd/openttd -D -c ${HOME}/.config/openttd/${1}.cfg'"
  screen -L -Logfile "${server_logfile}" -dmS ttd "${HOME}/openttd/openttd" -D -c "${HOME}/.config/openttd/${1}.cfg"
fi

# get and display newly-created PID
screen_pid="$(screen -S ttd -Q echo '$PID\n')"
echo "${cyan}Info::${nc}OpenTTD server started [${red}pid=${screen_pid}${nc}]"
echo "${cyan}Info::${nc}Use command 'screen -r ttd' to attach manually"
