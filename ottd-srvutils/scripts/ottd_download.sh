#!/bin/bash
# shellcheck source=/dev/null
source "${HOME}/ottd-srvutils/config.sh"

colorize

function content_present {
  # $1: content name (1rst field in CSV file)
  # $2: file_id (2nd field in CSV file)
  # $3: dl_id (3rd field in CSV file)
  # $4: lineno of CSV file
  local found
  found="no"
  for archive in ${newgrf_dir}/*
  do
    # match tar archive name with ID (second field in csv)
    if [[ "${archive}" =~ .*${2}.*.tar ]]
    then
      found="yes"
      echo -ne "\e[1A\e[K${cyan}Info::${nc}Content ${cur_content}/${total_content} ${yellow}${1}${nc} [file_id=${2}]-[dl_id=${3}]-${green}[INSTALLED]${nc}\n"
    fi
  done
  if [ "${found}" == "yes" ]
  then
    return 0
  else
    return 1
  fi
}

function uninstall {
  local content_ids
  content_ids="$(awk -F',' '{print $3}' ./content_ids.csv | tr '\n' ' ')"
  ./ottd_exec.sh "content update"
  for id in ${content_ids}
  do
    echo "${cyan}Info::${nc}Unselecting ${red}${id}${nc}"
    ./ottd_exec.sh "content unselect ${id}"
  done
  echo "${cyan}Info::${nc}Removing tar files in ${yellow}${newgrf_dir}${nc}"
  find "${newgrf_dir}" -iname "*.tar*" -delete
}

if no_server_running &>/dev/null
then
  echo "${red}Error::${nc}Server is not running. Use 'ottd_server.sh' to start it"
  exit 1
fi

# if CLI args contains --uninstall, uninstall and exit
until [ ${#@} -eq 0 ]
do
  [[ "${1}" =~ --uninstall ]] && { uninstall && exit 1; }
  shift
done

# remove potential trailing empty lines
sed -i '/^$/d' "${content_file}"
# cast all IDs to lowercase
tr '[:upper:]' '[:lower:]' < "${content_file}" > ./newcontent
mv ./newcontent "${content_file}"
total_content="$(cat ${content_file} | wc -l)"

echo "${cyan}Info::${nc}Reading content to download from ${yellow}${content_file}${nc}"
declare -i cur_content
cur_content=1
while IFS= read -r line
do
  sed -i '/^dbg:.*$/d' "${server_logfile}"
  content=(${line//,/ }) # create array split on equal sign
  dl_id="${content[2]}"
  ./ottd_exec.sh "content update"
  declare -i tries
  tries=1
  until content_present "${content[0]}" "${content[1]}" "${dl_id}" "${cur_content}"
  do
    echo -ne "${cyan}Info::${nc}Content ${cur_content}/${total_content} ${yellow}${content[0]}${nc} [file_id=${content[1]}]-[dl_id=${dl_id}]-${magenta}[DOWNLOADING (try=${tries})]...${nc}\n"
    sleep 1
    ./ottd_exec.sh "content select ${dl_id}"
    sleep 1
    ./ottd_exec.sh "content download"
    sleep 1
    ((tries=tries+1))
  done
  ((cur_content=cur_content+1))
done < "${content_file}"
