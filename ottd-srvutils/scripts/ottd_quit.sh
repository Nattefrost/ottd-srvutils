#!/bin/bash

# shellcheck source=/dev/null
source "${HOME}/ottd-srvutils/config.sh"

if ! no_server_running &>/dev/null
then
  screen -S ttd -p 0 -X stuff "quit\n"
fi
