#!/bin/bash

source ./config.sh

# get colors
colorize

function install_pkgs {
  echo "${cyan}Info::${nc}Installing dependencies through package manager"
  if [[ "${linux_distrib}" =~ debian|ubuntu ]]
  then
    # OpenTTD dependencies
    apt install -y fontconfig-config fonts-dejavu-core liblzo2-2 \
      libasyncns0 libflac8 libfontconfig1 libpulse0 \
      libsdl2-2.0-0 libsndfile1 libvorbisenc2 libwayland-client0 \
      libwayland-cursor0 libwayland-egl1 libxcursor1 libxfixes3 \
      libxi6 libxinerama1 libxkbcommon0 libxrandr2 libxrender1 libxss1 libxxf86vm1 x11-common libfluidsynth1  
    # Additional dependencies
    apt install -y wget unzip screen 
    apt install -y bats
  elif [[ "${linux_distrib}" =~ centos|redhat|fedora ]]
  then
    # OpenTTD dependencies
    dnf install -y fontconfig dejavu-fonts-common.noarch lzo \
      libasyncns flac-libs fontconfig-devel pulseaudio-libs \
      SDL libsndfile libvorbis libXcursor libXfixes libXi libXinerama \
      libxkbcommon libXrandr libXrender libXScrnSaver libXxf86vm libX11-common fluidsynth-libs
    dnf install -y libwayland-client libwayland-cursor libwayland-egl
    # Additional dependencies
    dnf install -y epel-release
    dnf install -y wget unzip screen bats
  fi
}

function install_ottd {
  echo "${cyan}Info::${nc}Downloading and installing OpenTTD & Opengfx"
  wget  "${openttd_url}" -O /tmp/openttd.tar.xz &>/dev/null
  tar xf "/tmp/openttd.tar.xz" -C "/home/${unix_user}/"
  wget  "${opengfx_url}" -O /tmp/opengfx.zip &>/dev/null
  unzip "/tmp/opengfx.zip" -d /home/${unix_user}/openttd-*/baseset/
  mv /home/${unix_user}/openttd-* /home/${unix_user}/openttd/
}

function copy_templates {
  echo "${cyan}Info::${nc}Copying OpenTTD template files to /home/${unix_user}/.config/openttd/"
  cp ./ottd-srvutils/configs/*.cfg /home/${unix_user}/.config/openttd/
}

function run_ottd_server_once {
  echo "${cyan}Info::${nc}Running OpenTTD server once to generate default openttd.cfg file"
  # run the server once in order to check that it works and create the initial config file & tree structure
  echo "${cyan}Info::${nc}Running openttd server once"
  su - "${unix_user}" -c "/bin/bash -c '/bin/bash /home/${unix_user}/ottd-srvutils/ottd-srvutils/scripts/ottd_server.sh'"
  sleep 10 # wait for the process to fully boot
  su - "${unix_user}" -c "/bin/bash -c '/bin/bash /home/${unix_user}/ottd-srvutils/ottd-srvutils/scripts/ottd_exec.sh \"quit\"'"
}

function setup_user {
  echo "${cyan}Info::${nc}Creating unix user ${unix_user} which will run OpenTTD server"
  if [[ "${linux_distrib}" =~ debian|ubuntu ]]
  then
    adduser "${unix_user}" --gecos '' --disabled-password --home "/home/${unix_user}"
    usermod -aG sudo "${unix_user}"
  elif [[ "${linux_distrib}" =~ centos|redhat|fedora ]]
  then
    adduser "${unix_user}"
    usermod -aG wheel "${unix_user}"
  fi
  chpasswd <<< "${unix_user}:${unix_user_pass}"
}

function copy_project_to_home {
  echo "${cyan}Info::${nc}Copying ottd-srvutils scripts to /home/${unix_user}/ottd-srvutils/scripts"
  mkdir -p "/home/${unix_user}/ottd-srvutils"
  cp -r ./* "/home/${unix_user}/ottd-srvutils/"
  chown -R "${unix_user}:" "/home/${unix_user}/ottd-srvutils/ottd-srvutils"
}

setup_user
copy_project_to_home
install_pkgs
install_ottd
run_ottd_server_once &>/dev/null
copy_templates

# restore proper rights
chown -R "${unix_user}:" "/home/${unix_user}/.config/openttd/"
chown -R "${unix_user}:" "/home/${unix_user}/"
chmod +x  /home/${unix_user}/ottd-srvutils/ottd-srvutils/scripts/*.sh
