# OTTD-srvutils
OTTD-srvutils is a collection of scripts to ease the use of an OpenTTD server. 
Main goal is to have an easier process to download the online content and have a few default configs with the most popular newgrfs.


## Installation

### Before Installing
**MANDATORY** Edit the `config.sh` script to change the password -and username if you wish to- of the UNIX user that will be created (or reuse an existing user).

### Actual installation
Run `install.sh` with root privileges -- **review what it does if you wish to** --
It will create a `ottd` user and install the 'generic binaries' version of OpenTTD in its home directory, install the opengfx baseset.
Both these archives are pulled from the *[official Download page](https://www.openttd.org/downloads/opengfx-releases/latest.html)*  
It will then put some default server configurations from `configs/` into `/home/ottd/.config/openttd/`
Some scripts will be put into `/home/ottd/ottd-srvutils/ottd-srvutils/scripts`

## Compatibility
This project was tested on the following GNU/Linux distributions
- [x] debian10  
- [x] ubuntu20.04  
- [x] CentOS8 Stream
- [ ] Redhat8
- [x] Fedora 33
- [ ] OpenSUSE  

### Dependencies
Only tested on debian10 & ubuntu20.04 as of may 2021.
```bash
# OpenTTD dependencies
apt install -y fontconfig-config fonts-dejavu-core liblzo2-2 \
    libasyncns0 libflac8 libfontconfig1 libpulse0 \
    libsdl2-2.0-0 libsndfile1 libvorbisenc2 libwayland-client0 \
    libwayland-cursor0 libwayland-egl1 libxcursor1 libxfixes3 \
    libxi6 libxinerama1 libxkbcommon0 libxrandr2 libxrender1 libxss1 libxxf86vm1 x11-common libfluidsynth1
# additional ottd-srvutils dependencies
apt install -y unzip screen wget
```

## Usage

### First launch
The first time you'll launch the OpenTTD server you'll probably want to download some online content (for details about that see [`online content`](#online-content) section).
So you'll need to start a server and then download :
```bash
cd /home/ottd/ottd-srvutils/ottd-srvutils/scripts/
bash ottd_server.sh
bash ottd_download.sh
```
![first_run_and_content_download](./doc/first_run.png "First run")

Here are the main features of ottd-srvutils.
- `ottd_server` will start the server, **by default it will run a vanilla config without mods**, it can take an optional parameter which is the name of the config file in `/home/ottd/.config/openttd/`; for instance to start the server with the *firs-arctic* template, use the following command -- make sure you have the correct content downloaded though, see next section --  : 
  - `./ottd_server firs-arctic`
  The server log will be in `$HOME/ottd-srvutils/server.log` by default. It can be changed in `config.sh`
- To download online content, see [`online content`](#online-content) section.
- To save the game currently running, use `ottd_exec.sh` with "save savename" ->  `./ottd_exec.sh "save mygame"`
- To load a game, in the same fashion use `./ottd_exec.sh "load mygame"`
- To stop a running server -> `./ottd_exec.sh "quit"`  

**NB: the ottd_exec.sh script executes commands into the OpenTTD server console so it supports all the**  *[console commands](https://wiki.openttd.org/en/Manual/Console%20Commands)*



## Online content

### Install
Edit the `content_ids.csv` file, it's a CSV file. It comes with a selection of some newgrf. 
 - First is the name you give to the content, it is only indicative and doesnt have to match the name on https://bananas.openttd.org
 - Second field is the ID, for example for FIRS v4 it can be seen in the content Id on this page (and the url) : https://bananas.openttd.org/package/newgrf/f1250008
 - Third field is the download id, it seems that it can not be found from the web page, you have to `content state <content name>` to find out, it's the first column. This is what the `ottd_download.sh` script uses to know which content it must select and download.  

Example from the default `content_ids.csv` file : 
>roadhog,9787eafe,9173679  
>firsv4,f1250008,3043328

To actually download the online content, run the `ottd_download.sh` script. It takes no parameter. It will read the `content_ids.csv` file and download based on the data in the third field (numerical ID) and download every module/newgrf listed in this file.  
**NB : The server must be running when downloading. See [`first launch`](#first-launch) section.**  


### Uninstall
To remove all content -- still based on the data in `content_ids.csv` -- run the same script `ottd_download.sh` witht he `--uninstall` argument
```bash
./ottd_download.sh --uninstall
Info::Unselecting 9173679
Info::Unselecting 3043328
Info::Unselecting 11515239
Info::Unselecting 2132461
Info::Unselecting 8333069
Info::Unselecting 13061457
Info::Unselecting 107988
Info::Unselecting 552692
Info::Unselecting 14229629
Info::Unselecting 13373903
Info::Unselecting 15270824
Info::Removing tar files in /home/ottd/.local/share/openttd/content_download/newgrf
```

## General advice & feedback
Review and edit the config template files in `/home/ottd/.config/openttd/` to adjust the game parameters to your preferences *[OpenTTD wiki](https://wiki.openttd.org/en/Archive/Manual/Settings/Openttd.cfg#snippet-post-104-nowiki)* can be useful for this. And dont forget to **CHANGE THE PASSWORDS** (blank by default) if needed.



