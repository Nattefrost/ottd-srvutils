#!/bin/bash

function get_distrib {
  local distrib_id
  distrib_id="$(grep -E '^ID=.*' /etc/os-release | tr -d 'ID=')"
  echo "${distrib_id}"
}

linux_distrib="$(get_distrib)"
unix_user=ottd
unix_user_pass=dummy
openttd_url=https://cdn.openttd.org/openttd-releases/12.2/openttd-12.2-linux-generic-amd64.tar.xz
opengfx_url=https://cdn.openttd.org/opengfx-releases/7.1/opengfx-7.1-all.zip
content_file="/home/${unix_user}/ottd-srvutils/ottd-srvutils/scripts/content_ids.csv"
newgrf_dir="${HOME}/.local/share/openttd/content_download/newgrf"
server_logfile="${HOME}/ottd-srvutils/server.log"

function no_server_running {
  local screen_pid
  screen_pid="$(screen -S ttd -Q echo '$PID\n')"
  if grep -q -E '[0-9]' <<< "${screen_pid}"
  then
    echo "${red}Error::${nc}An OpenTTD server process is already running [${red}pid=${screen_pid}${nc}], stop it first using ${green}./ottd_exec.sh \"quit\"${nc}"
    return 1
  else
    echo "${cyan}Info::${nc}No server running, station is free. CHOO CHOO !"
    return 0
  fi
}

function colorize {
  if test -t 1
  then
    ncolors=$(tput colors)
    if test -n "${ncolors}" && test ${ncolors} -ge 8
    then
      nc=$(tput sgr0)
      red=$(tput setaf 1)
      green=$(tput setaf 2)
      yellow=$(tput setaf 3)
      magenta=$(tput setaf 4)
      cyan=$(tput setaf 5)
    fi
  fi
}
