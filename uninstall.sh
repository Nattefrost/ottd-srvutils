#!/bin/bash

source ./config.sh

rm -rf "/home/${unix_user}/ottd-srvutils/"
rm -rf "/home/${unix_user}/openttd/"
rm -rf "${newgrf_dir}"
