#!/usr/bin/bats

source ../config.sh

function quit_server {
  cd /home/${unix_user}/ottd-srvutils/ottd-srvutils/scripts/
  bash ./ottd_quit.sh
}

function setup {
  quit_server
}

@test "Opengfx is installed" {
  command test -f /home/${unix_user}/openttd/baseset/opengfx-*
}

@test "OpenTTD is executable" {
  command test -x /home/${unix_user}/openttd/openttd
}

@test "Starting OpenTTD server" {
  cd "/home/${unix_user}/ottd-srvutils/ottd-srvutils/scripts/"
  run bash ottd_server.sh
  [ "$status" -eq 0 ]
}

@test "Stopping OpenTTD server" {
  run quit_server
}

@test "Starting the OpenTTD server twice should fail" {
  local regex
  regex='.*already running [pid=[[:digit:]]+]'
  run quit_server
  run bash ottd_server.sh
  run bash ottd_server.sh
  [ "${status}" -ne 0 ]
  [[ "${output}" =~ $regex ]]
}

#@test "Calling ottd_download.sh without a running server should fail" {
#  local regex
#  regex='.*Server is not running.*'
#  run quit_server
#  run bash ottd_download.sh
#  [ "${status}" -ne 0 ]
#  [[ "${output}" =~ $regex ]]
#}

function teardown {
  quit_server
}
